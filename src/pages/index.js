import React, { useState } from "react"

import { useSelector } from "react-redux"

import Layout from "../components/Layout"

import CurrentDate from "../components/CurrentDate.js"
import Popup from "../components/Popup"
import Calendar from "../components/Calendar"
import SleepTracker from "../components/SleepTracker"

const Home = () => {
  const data = useSelector(state => state.days.array)
  const [selectedDate, setSelectedDate] = useState("")
  const [popup, setPopup] = useState(true)
  return (
    <Layout>
      <CurrentDate />
      {popup && (
        <Popup selectedDate={selectedDate} setPopup={e => setPopup(e)} />
      )}
      <Calendar
        selectedDate={selectedDate}
        setSelectedDate={e => setSelectedDate(e)}
        setPopup={e => setPopup(e)}
        data={data}
      />
      <SleepTracker />
    </Layout>
  )
}

export default Home
