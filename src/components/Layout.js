import React from "react"

const Layout = ({children}) => {
  return (
    <>
      <header>Habit Tracker, Jonathan</header>
      <main>{children}</main>
      <footer>Made with ReactJS</footer>
    </>
  )
}

export default Layout
