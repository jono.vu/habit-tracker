import React from "react"

import moment from "moment"

import styles from "./css/currentdate.module.scss"

const CurrentDate = () => {
  const dateFormat = "dddd, Do MMMM YYYY"
  const currentDate = moment().format(dateFormat)

  return (
    <main className={styles.main}>
      <h3 className={styles.title}>Today is {currentDate}</h3>
    </main>
  )
}

export default CurrentDate
