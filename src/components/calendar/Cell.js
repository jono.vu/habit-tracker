import React from "react"

import moment from "moment"

import styles from "./css/cell.module.scss"

const Cell = props => {
  const data = props.data
  const sleepTime = data && moment(data.sleepTime, "hh:mm").format("h:mma")
  const wakeTime = data && moment(data.wakeTime, "hh:mm").format("h:mma")
  const minutesSlept =
    data &&
    moment(data.wakeTime, "hh:mm").diff(
      moment(data.sleepTime, "hh:mm"),
      "minutes"
    )
  const justifiedMinutesSlept =
    minutesSlept && minutesSlept < 0 ? minutesSlept + 1440 : minutesSlept
  const hoursSlept =
    justifiedMinutesSlept &&
    Math.floor(justifiedMinutesSlept / 60) +
      "h " +
      (justifiedMinutesSlept % 60 != 0
        ? (justifiedMinutesSlept % 60) + "m"
        : "")

  return (
    <div className={props.blank && styles.blank}>
      <div className={props.currentDate && styles.currentDate}>
        <div className={props.active && styles.active}>
          <div
            className={styles.cell}
            key={props.key}
            data-id={props.id}
            data-tag={props.tag}
            onClick={props.onClick}
          >
            <span className={styles.text}>
              {props.blank
                ? "Blank"
                : props.currentDate
                ? props.text + " ●"
                : props.text}
            </span>
            {data && <div className={styles.hours}>{hoursSlept}</div>}
            {data && (
              <div className={styles.info}>
                <p className={styles.label}>
                  <strong>Slept</strong> {sleepTime}
                </p>
                <p className={styles.label}>
                  <strong>Woke</strong> {wakeTime}
                </p>
                <p className={styles.label}>
                  {data.exercised && (
                    <>
                      <strong>Exercised </strong>{" "}
                      <div className={styles.check}>✓</div>
                    </>
                  )}
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Cell
