import React from "react"

import styles from "./css/header.module.scss"

const Button = props => {
  return (
    <button className={styles.button} onClick={props.onClick}>
      {props.text}
    </button>
  )
}

const Header = ({ currentMonth, setPrevMonth, setNextMonth }) => {
  return (
    <header className={styles.header}>
      <Button onClick={setPrevMonth} text={"<<"} />
      <span className={styles.currentMonth}>{currentMonth}</span>
      <Button onClick={setNextMonth} text={">>"} />
    </header>
  )
}

export default Header
