import React from "react"

import _ from "lodash"

import styles from "./css/cells.module.scss"

import Cell from "./Cell"

const Cells = props => {
  const blankDays = props.blankDays
  const monthDays = props.monthDays
  const handleClick = e => {
    props.selectDate(e)
    props.setPopup(true)
  }
  return (
    <section className={styles.cells}>
      {blankDays.map(i => {
        return <Cell blank={true} key={i} />
      })}
      {monthDays.map(i => {
        const data = _.find(props.data, { date: i.date })
        console.log(data)
        return (
          <Cell
            key={i.date}
            id={i.date}
            tag={false}
            onClick={handleClick}
            active={props.selectedDate === i.date && true}
            text={i.formattedNumber}
            currentDate={props.currentDate === i.date && true}
            data={data}
          />
        )
      })}
    </section>
  )
}

export default Cells
