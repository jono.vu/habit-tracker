import React from "react"
import moment from "moment"

import styles from "./css/days.module.scss"

const Days = ({ days, startWeek, dayFormat }) => {
  return (
    <header className={styles.header}>
      {days.map(i => {
        return (
          <div className={styles.day} key={i}>
            {moment(startWeek, dayFormat)
              .add(i, "days")
              .format(dayFormat)}
          </div>
        )
      })}
    </header>
  )
}

export default Days
