import React from "react"

import moment from "moment"

import { useDispatch } from "react-redux"
import { addDay, resetDays } from "../state/days"

import { Formik, Form, Field } from "Formik"

import Button from "./ui/Button"

import styles from "./css/popup.module.scss"

const Input = props => {
  return (
    <div className={styles.field}>
      <p className={styles.label}>{props.label}</p>
      <input {...props} className={styles.input} />
    </div>
  )
}

const Popup = props => {
  const dispatch = useDispatch()
  const currentDate = moment().format("dddd Do MMM YYYY")
  const date = props.selectedDate ? props.selectedDate : currentDate
  const setPopUpFalse = () => {
    props.setPopup(false)
  }
  return (
    <>
      <div className={styles.popup}>
        <header className={styles.header}>
          <p>{date}</p>
        </header>
        <Formik
          initialValues={{
            date: currentDate,
            sleepTime: "00:00",
            wakeTime: "09:00",
            exercised: false,
          }}
          onSubmit={values => {
            console.log(date)
            dispatch(
              addDay({
                date: date,
                sleepTime: values.sleepTime,
                wakeTime: values.wakeTime,
                exercised: values.exercised,
              })
            )
            setPopUpFalse
          }}
        >
          <Form>
            <Field
              label="What time did you sleep last night?"
              name="sleepTime"
              type="time"
              as={Input}
            />
            <Field
              label="What time did you wake up this morning?"
              name="wakeTime"
              type="time"
              as={Input}
            />
            <Field
              label="Did you exercise today?"
              name="exercised"
              type="checkbox"
              as={Input}
            />
            <Button type="submit" text="Submit" className={styles.button} />
          </Form>
        </Formik>
        {/* <button type="button" onClick={useDispatch(resetDays())}>
        Reset
      </button> */}
      </div>
      <div className={styles.bg} onClick={setPopUpFalse} />
    </>
  )
}

export default Popup
