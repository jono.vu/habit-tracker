import React, { useState, useEffect } from "react"

import moment from "moment"

import Header from "./calendar/Header"
import Days from "./calendar/Days"
import Cells from "./calendar/Cells"

import styles from "./css/calendar.module.scss"

const Calendar = props => {
  // Header Month Buttons

  const [currentMonth, setMonth] = useState()

  const monthFormat = "MMMM YYYY"

  useEffect(() => {
    setMonth(moment().format(monthFormat))
  }, [])

  const setPrevMonth = () => {
    setMonth(
      moment(currentMonth, monthFormat)
        .subtract(1, "M")
        .format(monthFormat)
    )
  }

  const setNextMonth = () => {
    setMonth(
      moment(currentMonth, monthFormat)
        .add(1, "M")
        .format(monthFormat)
    )
  }

  // Days Rendering

  const dayFormat = "ddd"

  const days = [0, 1, 2, 3, 4, 5, 6]

  const startWeek = moment()
    .startOf("week")
    .format(dayFormat)

  // Date Rendering

  const dateFormat = "dddd Do MMM YYYY"

  const count = moment(currentMonth, monthFormat).daysInMonth()

  const monthDays = []
  for (var i = 0; i < count; i++) {
    monthDays.push({
      formattedNumber: moment(currentMonth, monthFormat)
        .startOf("Month")
        .add(i, "days")
        .format("Do"),
      date: moment(currentMonth, monthFormat)
        .startOf("Month")
        .add(i, "days")
        .format(dateFormat),
    })
  }

  const currentDate = moment().format(dateFormat)

  const numberBlankDay = moment(currentMonth, monthFormat)
    .startOf("month")
    .weekday()

  const blankDays = []
  for (var i = 0; i < numberBlankDay; i++) {
    blankDays.push(i)
  }

  // Selecting Dates

  const selectDate = e => {
    const res = e.target.getAttribute("data-id")
    props.setSelectedDate(res)
    console.log("Date selected: " + props.selectedDate)
  }

  return (
    <section className={styles.calendar}>
      <Header
        currentMonth={currentMonth}
        setPrevMonth={setPrevMonth}
        setNextMonth={setNextMonth}
      />
      <Days days={days} startWeek={startWeek} dayFormat={dayFormat} />
      <Cells
        data={props.data}
        blankDays={blankDays}
        monthDays={monthDays}
        selectDate={e => selectDate(e)}
        selectedDate={props.selectedDate}
        currentDate={currentDate}
        setPopup={props.setPopup}
      />
    </section>
  )
}

export default Calendar
