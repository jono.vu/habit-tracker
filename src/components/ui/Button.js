import React from "react"

import styles from "./css/button.module.scss"

const Button = props => {
  return (
    <div className={props.className}>
      <button
        className={styles.button}
        onClick={props.onClick}
        type={props.type}
      >
        {props.text}
      </button>
    </div>
  )
}

export default Button
