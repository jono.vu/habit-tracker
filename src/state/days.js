import { findIndex } from "lodash"
import { createSlice } from "redux-starter-kit"

const daysSlice = createSlice({
  name: "days",
  initialState: { array: [] },
  reducers: {
    addDay(state, action) {
      const index = findIndex(state.array, { date: action.payload.date })
      if (index < 0) {
        console.log("Info Added for this date!")
        state.array = [...state.array, action.payload]
      } else {
        console.log("Info Updated for this date!")
        state.array = Object.assign([], state.array, {
          [index]: action.payload,
        })
      }
    },
    resetDays(state) {
      state.array = []
    },
  },
})
// Extract the action creators object and the reducer
const { actions, reducer } = daysSlice

export const { addDay, resetDays } = actions

export default reducer
