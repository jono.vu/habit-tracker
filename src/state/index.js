import { persistReducer, persistStore } from "redux-persist"
import storage from "redux-persist/lib/storage"
import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from "redux-starter-kit"

import days from "./days"

const reducer = combineReducers({
  days,
})

const persistConfig = {
  key: "root",
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducer)

export default function createStore(preloadedState) {
  const store = configureStore({
    reducer: persistedReducer,
    preloadedState,
    middleware: getDefaultMiddleware({
      serializableCheck: false,
    }),
  })
  const persistor = persistStore(store)
  return { store, persistor }
}
